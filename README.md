# README #

Start the server using
python server.py

Then visit page http://localhost:5000

### What is this repository for? ###

Book seats for the Lion King show

Calendar data is at https://tw.igs.farm/lionking/all.json
Seat details for a single performance is at https://tw.igs.farm/lionking/692A2B01-A607-4616-9409-9696D905340D (for example)

