from flask import Flask, render_template,redirect

app = Flask(__name__)

@app.route("/")
def index():
    return redirect('/lionking/2022-01-01',302)

@app.route("/lionking/<date>")
def calendar(date):
    return render_template("calendar.html")

app.run(debug=True)
